"use strict";

import BaseComponent from '../base-component/BaseComponent.mjs'

export default class Router extends BaseComponent {

  template = '@text placeholder'
  get fallbackComponent() {
    const that = this
    return class FallbackPage extends BaseComponent {
      static forceOpenShadow = Router.forceOpenShadow
      template() {
        return `
          p
            @text Page "${that.win.document.location.hash.substr(1)}" not found.
        `
      }
    }
  }

  get win() {
    return window.Cypress ? window.innerWindow : window
  }

  constructor() {
    super()
    this.win.addEventListener('hashchange', this.__matchRoute.bind(this))
  }

  __afterCreate() {
    this.__matchRoute()
  }

  afterConnected() {

  }

  attributeChangedCallback(name, oldValue, newValue) {

  }

  __routeToRegExp(routeString) {
    return new RegExp(routeString.replace(/\{\s*([\w]+)\s*\}/gsm, '(?<$1>[^/]*)'), 'sm')
  }

  __matchRoute() {
    const routes = Object.entries(JSON.parse(this.dataset.routes || '{}'))
      .map(([routeStr, component]) => {
        return [
          routeStr,
          this.__routeToRegExp(routeStr),
          window.Cypress ? this.win[component] : component
        ]
      })
    const [ path, queryString ] = this.win.document.location.hash.substr(1).split('?')

    const componentTriplex = routes.find(([routeStr, re]) => path.match(re))

    const Component = componentTriplex ? componentTriplex[2] : this.fallbackComponent
    const params = componentTriplex ? { ...path.match(componentTriplex[1]).groups } : {}
    const queryParams = (queryString || '').split('&').reduce((memory, current) => {
      let [ key, val ] = current.split('=')
      if (key.match(/\[\]$/)) {
        key = key.replace(/\[\]$/, '')
        memory[key] = [...(memory[key] || []), val]
        return memory
      }

      return {...memory, [key]: val}
    }, {})
    console.log(`Route to "${path}":`, Component.name, params, queryParams)

    this.loadComponent(Component, {params, queryParams})
      .then(ComponentWrapper => {
        const component = ComponentWrapper.create()
        while (this.root.firstChild) this.root.firstChild.remove()
        this.root.appendChild(component)
      })
      .catch(err => {
        console.error(`Router fail to load path "${path}"`, routes[path], err)
        //alert('Router fail to load component for '+ path +'\n\n'+ err.message)
      })
  }

}
