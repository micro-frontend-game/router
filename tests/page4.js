import BaseComponent from '../node_modules/base-component/BaseComponent.mjs'

export default class Page4 extends BaseComponent {
  template() {
    return `
    p
      @text Parameters "${this.shared.params.param1 || ''}" and "${this.shared.params.param2 || ''}" received. ${
        Object.keys(this.shared.queryParams).length
        ? `Also some query params: ${JSON.stringify(this.shared.queryParams)}`
        : 'With no query params.'
      }
    `
  }
}
