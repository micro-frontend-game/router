import BaseComponent from '../node_modules/base-component/BaseComponent.mjs'

export default class Page3 extends BaseComponent {
  template() {
    return `
    p
      @text Parameter "${this.shared.params.param1 || ''}" received. ${
        Object.keys(this.shared.queryParams).length
        ? `Also some query params: ${JSON.stringify(this.shared.queryParams)}`
        : 'With no query params.'
      }
    `
  }

}
