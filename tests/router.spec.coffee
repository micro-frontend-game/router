import loadComponent           from '../node_modules/loader/loader.mjs'
import naiveML                 from '../node_modules/naiveML/naiveML.mjs'
import { build as naiveBuild } from '../node_modules/naiveML/naiveML.mjs'
import Page1                   from './page1.js'
import Page2                   from './page2.js'
import Page3                   from './page3.js'
import Page4                   from './page4.js'
import Page404                 from './page404.js'
import RouterBase              from '../router.mjs'

RouterBase.forceOpenShadow = true
Page1.forceOpenShadow      = true
Page2.forceOpenShadow      = true
Page3.forceOpenShadow      = true
Page4.forceOpenShadow      = true
Page404.forceOpenShadow    = true

describe 'Routing micro-component', ->
  before () -> Cypress.config 'includeShadowDom', true

  beforeEach ->
    cy.visit('/')
    cy.wait(1).then () =>
      loadComponent(RouterBase).then (@Router) =>
    cy.window().then (@win) =>
      @doc = @win.document
      @doc.body.innerHTML = ''
      window.innerWindow = @win

  it 'builds', ->
    @Router.create parent: @doc.body

  it 'renders a component based on a route', ->
    @win.Page1   = Page1
    @win.Page2   = Page2
    @win.Page404 = Page404

    @Router.create parent: @doc.body, 'data-routes': '{
      "/test1": "Page1",
      "/test2": "Page2",
      ".*": "Page404"
    }'

    cy.contains 'Page "" does not exists.'
    cy.wait(0).then () => @doc.location.hash = '/test1'
    cy.contains 'Page Test 1 AAA'
    cy.wait(0).then () => @doc.location.hash = '/test2'
    cy.contains 'Page Test 2 BBB'

  it 'renders a fallback component if there is no match for route', ->
    @Router.create parent: @doc.body, 'data-routes': '{}'

    cy.contains 'Page "" not found.'

    cy.wait(0).then () => @doc.location.hash = '/test1'
    cy.contains 'Page "/test1" not found.'

  it 'passes parameters from the URL to the component', ->
    @win.Page3   = Page3
    @win.Page4   = Page4

    @Router.create parent: @doc.body, 'data-routes': '{
      "/test3/{param1}": "Page3",
      "/test4/{param1}/{param2}": "Page4"
    }'

    cy.wait(0).then () => @doc.location.hash = '/test3/bli'
    cy.contains 'Parameter "bli" received.'
    cy.wait(0).then () => @doc.location.hash = '/test4/foo/bar'
    cy.contains 'Parameters "foo" and "bar" received.'

  it 'passes query parameters from the URL to the component', ->
    @win.Page3 = Page3
    @win.Page4 = Page4

    @Router.create parent: @doc.body, 'data-routes': '{
      "/test5/": "Page3",
      "/test6/{param1}/{param2}": "Page4"
    }'

    cy.wait(0).then () => @doc.location.hash = '/test5/bli?param1=42'
    cy.contains 'Parameter "" received. Also some query params: {"param1":"42"}'
    cy.wait(0).then () => @doc.location.hash = '/test5/bli?arr[]=A&arr[]=B'
    cy.contains 'Parameter "" received. Also some query params: {"arr":["A","B"]}'
    cy.wait(0).then () => @doc.location.hash = '/test6/foo/bar?param1=171&param2=69'
    cy.contains 'Parameters "foo" and "bar" received. Also some query params: {"param1":"171","param2":"69"}'
