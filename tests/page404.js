import BaseComponent from '../node_modules/base-component/BaseComponent.mjs'

export default class Page404 extends BaseComponent {
  template() {
    return `
      p
        @text Page "${window.document.location.hash.substr(1)}" does not exists.
    `
  }
}
